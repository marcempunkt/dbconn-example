use std::env;
use std::sync::{ Arc, Mutex };
use actix_web::{ HttpServer,
                 App,
                 HttpResponse,
                 web };
use sqlx::Error as SqlxError;
use sqlx::mysql::{ MySqlPool,
                   MySqlPoolOptions,
                   MySqlRow };
use serde::{ Serialize, Deserialize };

struct AppState {
    pool: Mutex<MySqlPool>,
}

#[derive(Serialize, Deserialize)]
struct Response {
    message: String,
}

#[derive(Serialize, Deserialize)]
struct GetUsersResponse {
    message: String,
    users: Vec<User>,
}

#[derive(sqlx::FromRow, Debug, Serialize, Deserialize)]
struct User {
    id: i64,
    username: String,
    email: String,
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {

    const DATABASE_URL: &str = "mysql://user:password@127.0.0.1:3306/dbconn";
    env::set_var("DATABASE_URL", DATABASE_URL);

    let pool: MySqlPool = MySqlPoolOptions::new()
        .max_connections(10)
        .connect(DATABASE_URL)
        .await
        .unwrap();

    let app_state: web::Data<AppState> = web::Data::new(AppState {
        pool: Mutex::new(pool),
    });

    HttpServer::new(move || {
        App::new()
            .app_data(app_state.clone())
            .route("/", web::get().to(index))
            .route("/get_users", web::get().to(get_users))
    }).bind(("127.0.0.1", 4000))?
        .run()
        .await
}

async fn index() -> HttpResponse {
    HttpResponse::Ok().json(Response { message: "sqlx_state's server is running".to_string() })
}

async fn get_users(app_state: web::Data<AppState>) -> HttpResponse {
    let pool: &MySqlPool = &*app_state.pool.lock().unwrap();

    // let users = sqlx::query_as::<_, User>("SELECT * FROM users").fetch(pool);
    // let users: Result<Vec<User>, SqlxError> = sqlx::query!("SELECT * FROM users").fetch_all(pool).await; 
    // let records = sqlx::query!("SELECT id, username, email FROM users").fetch_all(pool).await.unwrap(); // returns Record
    // let users: Result<Vec<User>, SqlxError> = sqlx::query_as::<_, User>("SELECT * FROM users").fetch_all(pool).await;
    let _users: Result<Vec<User>, SqlxError> = sqlx::query_as!(User, "SELECT * FROM users").fetch_all(pool).await;
    // let rows: Result<Vec<MySqlRow>, SqlxError> = sqlx::query("SELECT * FROM users").fetch_all(pool).await;

    HttpResponse::Ok().json(GetUsersResponse {
        message: "Got all users.".to_string(),
        users: _users.unwrap(),
    })
}
