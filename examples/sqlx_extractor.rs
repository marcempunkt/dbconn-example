use std::env;
use std::future::{ Ready, ready };
use tokio::runtime::Runtime;
use actix_web::{ HttpServer,
                 App,
                 HttpResponse,
                 HttpRequest,
                 FromRequest,
                 Error as ActixWebError,
                 dev::Payload,
                 error,
                 web };
use sqlx::Error as SqlxError;
use sqlx::Connection;
use sqlx::mysql::{ MySqlConnection, };
use serde::{ Serialize, Deserialize };

#[derive(Serialize, Deserialize)]
struct Response {
    message: String,
}

#[derive(Serialize, Deserialize)]
struct GetUsersResponse {
    message: String,
    users: Vec<User>,
}

#[derive(sqlx::FromRow, Debug, Serialize, Deserialize)]
struct User {
    id: i64,
    username: String,
    email: String,
}

struct Db {
    conn: MySqlConnection,
}

impl FromRequest for Db {
    type Error = ActixWebError;
    type Future = Ready<Result<Self, Self::Error>>;

    fn from_request(_: &HttpRequest, _: &mut Payload) -> Self::Future {

        // let rt: Runtime = Runtime::new().unwrap();

        Box::pin(async move {
            Ok(Db { conn: conn.unwrap() })
        });

        let conn: Result<MySqlConnection, SqlxError> = tokio::spawn(async {
            MySqlConnection::connect(
                &env::var("DATABASE_URL").unwrap()
            ).await
        });

        if let Err(e) = conn {
            return ready(Err(error::ErrorInternalServerError(e.to_string())));
        }

        ready(Ok(Db { conn: conn.unwrap() }))
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {

    const DATABASE_URL: &str = "mysql://user:password@127.0.0.1:3306/dbconn";
    env::set_var("DATABASE_URL", DATABASE_URL);

    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(index))
            .route("/get_users", web::get().to(get_users))
    }).bind(("127.0.0.1", 4000))?
        .run()
        .await
}

async fn index() -> HttpResponse {
    HttpResponse::Ok().json(Response { message: "sqlx_extractor's server is running".to_string() })
}

async fn get_users(db: Db) -> HttpResponse {
    HttpResponse::Ok().json(GetUsersResponse {
        message: "Got all users.".to_string(),
        users: vec![User{ id: 1, username: "test".to_string(), email: "test".to_string() }],
    })
}
