#!/bin/bash

docker run --name dbconn \
       -p 3306:3306 \
       -e MARIADB_ROOT_USER=admin \
       -e MARIADB_ROOT_PASSWORD=password \
       -e MARIADB_DATABASE=dbconn \
       -v $PWD/init.sql:/docker-entrypoint-initdb.d/init.sql \
       -d mariadb:latest

exit 0;
echo "Wait one minute for the mysql socket to properly start..."
sleep 60s

echo "Creating Database..."
docker cp src/db/create_babbelndb.sql babbelndb:/home/create_babbelndb.sql
docker exec -it babbelndb /bin/sh -c 'mysql -u root --password=mypass < /home/create_babbelndb.sql'

echo "Creating Database was successfull!"
